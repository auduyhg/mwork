package com.example.company.mwork.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.company.mwork.BR;

/**
 * Created by Khuong Duy on 11/29/2017.
 */

public class LanguageSpokenViewModel extends BaseObservable {
    private int icon;
    private String language;
    private int code;
    private boolean isChecked = false;

    public LanguageSpokenViewModel(int icon, String language, int code) {
        this.icon = icon;
        this.language = language;
        this.code = code;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
    @Bindable
    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
        notifyPropertyChanged(BR.checked);
    }
}
