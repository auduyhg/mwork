package com.example.company.mwork.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.company.mwork.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.databinding.ItemConfirmWorkingDayBinding;
import com.example.company.mwork.databinding.ItemLanguageSpokenBinding;
import com.example.company.mwork.viewmodel.ConfirmWorkingDayViewModel;

import java.util.List;

/**
 * Created by Khuong Duy on 12/12/2017.
 */

public class ConfirmWorkingDayArrayAdapter extends ArrayAdapter<ConfirmWorkingDayViewModel> {
    protected final LayoutInflater layoutInflater;
    private Context context;
    private List<ConfirmWorkingDayViewModel> objects;

    public ConfirmWorkingDayArrayAdapter(@NonNull Context context, int resource, @NonNull List<ConfirmWorkingDayViewModel> objects) {
        super(context, resource, objects);
        this.context = context;
        //Get system layout inflate
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ItemConfirmWorkingDayBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_confirm_working_day, parent, false);
        binding.setVariable(BR.viewModel, objects.get(position));
        Log.e("position"," "+ position);
        return binding.getRoot();
    }

    public List<ConfirmWorkingDayViewModel> getObjects() {
        return objects;
    }
}
