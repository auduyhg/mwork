package com.example.company.mwork.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableList;

import java.util.Observable;

/**
 * Created by Khuong Duy on 12/11/2017.
 */

public class JobViewModel extends BaseObservable{
    private String jobId;
    private String jobName;
    private String jobDes;
    private int jobType;
    private int workingTimeType;
    private double salary;
    private double totalHour;
    private String jobAddress;
    private String startDate;
    private String endDate;
    private String companyId;
    private String companyName;
    private int status;
    private ObservableList<JobInDayViewModel> jobInDayViewModels;

    public JobViewModel(String jobId, String jobName, String jobDes, int jobType, int workingTimeType, double salary, double totalHour, String jobAddress, String startDate, String endDate, String companyId, String companyName, int status) {
        this.jobId = jobId;
        this.jobName = jobName;
        this.jobDes = jobDes;
        this.jobType = jobType;
        this.workingTimeType = workingTimeType;
        this.salary = salary;
        this.totalHour = totalHour;
        this.jobAddress = jobAddress;
        this.startDate = startDate;
        this.endDate = endDate;
        this.companyId = companyId;
        this.companyName = companyName;
        this.status = status;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
    @Bindable
    public int getWorkingTimeType() {
        return workingTimeType;
    }

    public void setWorkingTimeType(int workingTimeType) {
        this.workingTimeType = workingTimeType;
    }

    @Bindable
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Bindable
    public String getJobDes() {
        return jobDes;
    }

    public void setJobDes(String jobDes) {
        this.jobDes = jobDes;
    }

    @Bindable
    public int getJobType() {
        return jobType;
    }

    public void setJobType(int jobType) {
        this.jobType = jobType;
    }

    @Bindable
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Bindable
    public double getTotalHour() {
        return totalHour;
    }

    public void setTotalHour(double totalHour) {
        this.totalHour = totalHour;
    }

    @Bindable
    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    @Bindable
    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Bindable
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Bindable
    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Bindable
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Bindable
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public ObservableList<JobInDayViewModel> getJobInDayViewModels() {
        return jobInDayViewModels;
    }

    public void setJobInDayViewModels(ObservableList<JobInDayViewModel> jobInDayViewModels) {
        this.jobInDayViewModels = jobInDayViewModels;
    }
}
