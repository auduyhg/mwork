package com.example.company.mwork.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.adapter.ConfirmWorkingDayArrayAdapter;
import com.example.company.mwork.databinding.FragmentJobDetailBinding;
import com.example.company.mwork.utils.Constrant;
import com.example.company.mwork.viewmodel.JobViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobDetailFragment extends BaseFragment<FragmentJobDetailBinding, JobViewModel> {


    public JobDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public int getVariableId() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_job_detail;
    }

    @Override
    public JobViewModel setViewModel() {
        return Constrant.DUMMY_LIST_JOBS.get(0);
    }

    @Override
    public void onCreateFragment(Context context) {
        getBinding().lvConFirmWorkingDay.setAdapter(new ConfirmWorkingDayArrayAdapter(getActivity(),R.layout.item_confirm_working_day,Constrant.CONFIRM_WORKING_DAY_VIEW_MODEL_LIST));
    }

}
