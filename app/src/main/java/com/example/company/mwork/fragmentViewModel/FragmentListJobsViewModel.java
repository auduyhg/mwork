package com.example.company.mwork.fragmentViewModel;

import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.example.company.mwork.adapter.BaseSingleTypeRecyclerViewAdapter;
import com.example.company.mwork.utils.Constrant;
import com.example.company.mwork.viewmodel.JobViewModel;

/**
 * Created by Khuong Duy on 12/11/2017.
 */

public class FragmentListJobsViewModel extends BaseObservable {
    private ObservableList<JobViewModel> listOb = new ObservableArrayList<>();
    private BaseSingleTypeRecyclerViewAdapter<JobViewModel> adapter;

    public ObservableList<JobViewModel> getListOb() {
        return listOb;
    }

    public void setListOb(ObservableList<JobViewModel> listOb) {
        this.listOb = listOb;
        notifyChange();
    }

    public BaseSingleTypeRecyclerViewAdapter<JobViewModel> getAdapter() {
        return adapter;
    }

    public void setAdapter(BaseSingleTypeRecyclerViewAdapter<JobViewModel> adapter) {
        this.adapter = adapter;
        adapter.set(Constrant.DUMMY_LIST_JOBS);
    }
}
