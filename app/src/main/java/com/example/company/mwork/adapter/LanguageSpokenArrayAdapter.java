package com.example.company.mwork.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.company.mwork.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.databinding.ItemLanguageSpokenBinding;
import com.example.company.mwork.viewmodel.LanguageSpokenViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khuong Duy on 11/29/2017.
 */

public class LanguageSpokenArrayAdapter extends ArrayAdapter<LanguageSpokenViewModel> {
    protected final LayoutInflater layoutInflater;
    private Context context;
    private List<LanguageSpokenViewModel> objects;


    public LanguageSpokenArrayAdapter(@NonNull Context context, int resource, @NonNull List<LanguageSpokenViewModel> objects) {
        super(context, resource, objects);
        this.context = context;
        //Get system layout inflate
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ItemLanguageSpokenBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_language_spoken, parent, false);
        binding.setVariable(BR.viewModel, objects.get(position));
        return binding.getRoot();
    }

    public List<LanguageSpokenViewModel> getObjects() {
        return objects;
    }

    public List<Integer> getListLanguageChecked() {
        List<Integer> languages = new ArrayList<>();
        for (LanguageSpokenViewModel object : objects) {
            if (object.isChecked()) {
                languages.add(object.getCode());
            }
        }
        return languages;
    }
}
