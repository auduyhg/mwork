package com.example.company.mwork.utils;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.example.company.mwork.R;
import com.example.company.mwork.viewmodel.ConfirmWorkingDayViewModel;
import com.example.company.mwork.viewmodel.JobViewModel;
import com.example.company.mwork.viewmodel.LanguageSpokenViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khuong Duy on 11/29/2017.
 */

public class Constrant {
    public static final List<LanguageSpokenViewModel> LANGUAGE_SPOKEN_LIST = new ArrayList<LanguageSpokenViewModel>() {{
        add(new LanguageSpokenViewModel(R.drawable.flag_vietnam, "VietNam", 84));
        add(new LanguageSpokenViewModel(R.drawable.flag_united_states, "English", 01));
        add(new LanguageSpokenViewModel(R.drawable.flag_china, "Chinese", 33));
        add(new LanguageSpokenViewModel(R.drawable.flag_south_korea, "Korea", 55));
        add(new LanguageSpokenViewModel(R.drawable.flag_russia, "Russia", 104));
        add(new LanguageSpokenViewModel(R.drawable.flag_japan, "Japan", 69));
        add(new LanguageSpokenViewModel(R.drawable.flag_more, "Other", 00));
    }};

    public static final ObservableList<JobViewModel> DUMMY_LIST_JOBS = new ObservableArrayList<JobViewModel>() {{
        add(new JobViewModel("id001", "Kitchen Stuff at Hilton Hanoi", "Kitchen Stuff at Hilton Hanoi is the best jobs for you",
                1, 1, 600000, 40, "Số 1, Lê Thánh Tông,Hà nội", "4 Dec 2017",
                "14 Dec 2017", "company1", "Hilton Hanoi", 1));
        add(new JobViewModel("id001", "Kitchen Stuff at Hilton Hanoi", "Kitchen Stuff at Hilton Hanoi is the best jobs for you",
                1, 1, 600000, 40, "Số 1, Lê Thánh Tông,Hà nội", "4 Dec 2017",
                "14 Dec 2017", "company1", "Hilton Hanoi", 1));
        add(new JobViewModel("id001", "Kitchen Stuff at Hilton Hanoi", "Kitchen Stuff at Hilton Hanoi is the best jobs for you",
                1, 1, 600000, 40, "Số 1, Lê Thánh Tông,Hà nội", "4 Dec 2017",
                "14 Dec 2017", "company1", "Hilton Hanoi", 1));
        add(new JobViewModel("id001", "Kitchen Stuff at Hilton Hanoi", "Kitchen Stuff at Hilton Hanoi is the best jobs for you",
                1, 1, 600000, 40, "Số 1, Lê Thánh Tông,Hà nội", "4 Dec 2017",
                "14 Dec 2017", "company1", "Hilton Hanoi", 1));
        add(new JobViewModel("id001", "Kitchen Stuff at Hilton Hanoi", "Kitchen Stuff at Hilton Hanoi is the best jobs for you",
                1, 1, 600000, 40, "Số 1, Lê Thánh Tông,Hà nội", "4 Dec 2017",
                "14 Dec 2017", "company1", "Hilton Hanoi", 1));
        add(new JobViewModel("id001", "Kitchen Stuff at Hilton Hanoi", "Kitchen Stuff at Hilton Hanoi is the best jobs for you",
                1, 1, 600000, 40, "Số 1, Lê Thánh Tông,Hà nội", "4 Dec 2017",
                "14 Dec 2017", "company1", "Hilton Hanoi", 1));
        add(new JobViewModel("id001", "Kitchen Stuff at Hilton Hanoi", "Kitchen Stuff at Hilton Hanoi is the best jobs for you",
                1, 1, 600000, 40, "Số 1, Lê Thánh Tông,Hà nội", "4 Dec 2017",
                "14 Dec 2017", "company1", "Hilton Hanoi", 1));
    }};

    public static final List<ConfirmWorkingDayViewModel> CONFIRM_WORKING_DAY_VIEW_MODEL_LIST = new ArrayList<ConfirmWorkingDayViewModel>() {{
        add(new ConfirmWorkingDayViewModel());
        add(new ConfirmWorkingDayViewModel());
        add(new ConfirmWorkingDayViewModel());
        add(new ConfirmWorkingDayViewModel());
        add(new ConfirmWorkingDayViewModel());
    }};
}
