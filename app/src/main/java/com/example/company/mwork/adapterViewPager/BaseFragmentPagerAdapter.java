package com.example.company.mwork.adapterViewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.company.mwork.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khuong Duy on 11/29/2017.
 */

public class BaseFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> list = new ArrayList<>();

    public BaseFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return list.get(position).getFragmentTitle();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public void setList(List<BaseFragment> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
