package com.example.company.mwork.activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.activityViewModel.ActivityRegisterViewModel;
import com.example.company.mwork.databinding.ActivityRegisterBinding;
import com.example.company.mwork.fragment.RegisterOneFragment;
import com.example.company.mwork.fragment.RegisterTwoFragment;

public class RegisterActivity extends BaseActivity<ActivityRegisterBinding, ActivityRegisterViewModel> implements View.OnClickListener {


    @Override
    public int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    public ActivityRegisterViewModel setViewModel() {
        return new ActivityRegisterViewModel(getSupportFragmentManager());
    }

    @Override
    public int getVariableId() {
        return BR.viewModel;
    }

    @Override
    public void onCreateActivity() {
        setStatusBarTranslucent(true);
        getSupportActionBar().hide();
        getBinding().vpRegisterStep.setAdapter(getViewModel().getPagerAdapter());
        getBinding().vpRegisterStep.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    getViewModel().setShowCreateButton(true);
                } else {
                    getViewModel().setShowCreateButton(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        getBinding().vpRegisterStep.beginFakeDrag();
        getBinding().btnNext.setOnClickListener(this);
        ((RegisterOneFragment) getBinding().getViewModel().getPagerAdapter().getItem(0)).setImeAction(new OnNextStep());

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNext:
                if (getViewModel().isShowCreateButton() && ((RegisterTwoFragment) getBinding().getViewModel().getPagerAdapter().getItem(1)).isValidate()) {
                    Intent intent = new Intent(RegisterActivity.this, StartActivity.class);
                    startActivity(intent);
                } else if (getViewModel().isShowCreateButton()) {
                    Toast.makeText(this, "Make sure you complete all fields", Toast.LENGTH_SHORT).show();
                } else if (((RegisterOneFragment) getBinding().getViewModel().getPagerAdapter().getItem(0)).isValidate()) {
                    getBinding().vpRegisterStep.setCurrentItem(1);
                } else {
                    Toast.makeText(this, "You must complete this step first", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public class OnNextStep implements RegisterOneFragment.OnImeAction {
        @Override
        public void onImeActionDone() {
            getBinding().vpRegisterStep.setCurrentItem(1);
        }
    }

    @Override
    public void onBackPressed() {
        if (getBinding().vpRegisterStep.getCurrentItem() == 1) {
            getBinding().vpRegisterStep.setCurrentItem(0);
        } else {
            super.onBackPressed();
        }
    }
}
