package com.example.company.mwork.utils;

/**
 * Created by Khuong Duy on 11/30/2017.
 */

public class ValidationPassword implements Validation {
    public static final int MIN_PASSWORD_LENGHT = 8;

    @Override
    public String getMessage() {
        return "Minimum password length is " + MIN_PASSWORD_LENGHT;
    }

    @Override
    public boolean validate(String input) {
        return (input.length() >= MIN_PASSWORD_LENGHT);
    }
}
