package com.example.company.mwork.fragmentViewModel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.widget.ArrayAdapter;

import com.example.company.mwork.R;

/**
 * Created by Khuong Duy on 11/29/2017.
 */

public class FragmentRegisterOneViewModel extends BaseObservable {
    private ArrayAdapter<String> arrayAdapter;
    private static final String[] listGender = {"Male", "Female", "Orther"};

    public FragmentRegisterOneViewModel(Context context) {
        arrayAdapter = new ArrayAdapter<String>(context, R.layout.gender_spinner_item, listGender);
        arrayAdapter.setDropDownViewResource(R.layout.gender_spinner_item);
    }

    public ArrayAdapter<String> getArrayAdapter() {
        return arrayAdapter;
    }

    public void setArrayAdapter(ArrayAdapter<String> arrayAdapter) {
        this.arrayAdapter = arrayAdapter;
    }
}
