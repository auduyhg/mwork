package com.example.company.mwork.fragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.android.databinding.library.baseAdapters.BR;
import com.example.company.mwork.R;

import com.example.company.mwork.databinding.FragmentRegisterOneBinding;
import com.example.company.mwork.fragmentViewModel.FragmentRegisterOneViewModel;
import com.example.company.mwork.utils.ValidationEmail;
import com.example.company.mwork.utils.ValidationPhone;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterOneFragment extends BaseFragment<FragmentRegisterOneBinding, FragmentRegisterOneViewModel> implements TextView.OnEditorActionListener, View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private OnImeAction imeAction;
    private Calendar calendar = Calendar.getInstance();

    public RegisterOneFragment() {
        // Required empty public constructor
    }

    @Override
    public int getVariableId() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_register_one;
    }

    @Override
    public FragmentRegisterOneViewModel setViewModel() {
        return new FragmentRegisterOneViewModel(getActivity());
    }

    @Override
    public void onCreateFragment(Context context) {
        getBinding().spGender.setAdapter(getViewModel().getArrayAdapter());
        getBinding().edPhoneNumber.setOnEditorActionListener(this);
        getBinding().edDOB.setOnClickListener(this);
        setupValidate();
    }

    private void setupValidate() {
        getBinding().edEmail.setupWithTextInputLayout(getBinding().tilEmail);
        getBinding().edPhoneNumber.setupWithTextInputLayout(getBinding().tilPhoneNumber);
        getBinding().edEmail.addValidation(new ValidationEmail());
        getBinding().edPhoneNumber.addValidation(new ValidationPhone());
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN
                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            hideKeyboard(getBinding().edPhoneNumber);
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edDOB:
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DatePickder, this, year, month, day);
                datePickerDialog.show();
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        getBinding().edDOB.setText(dayOfMonth + "/" + month + "/" + year);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    }

    public interface OnImeAction {
        void onImeActionDone();
    }

    public void setImeAction(OnImeAction imeAction) {
        this.imeAction = imeAction;
    }

    public void hideKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public boolean isValidate(){
        if(getBinding().edEmail.isValidate()
                && getBinding().edName.isValidate()
                && !getBinding().edDOB.getText().toString().isEmpty()
                && getBinding().edPhoneNumber.isValidate()){
            return true;
        }
        return false;
    }
}
