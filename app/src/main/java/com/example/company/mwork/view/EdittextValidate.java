package com.example.company.mwork.view;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import com.example.company.mwork.utils.Validation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khuong Duy on 11/30/2017.
 */

public class EdittextValidate extends android.support.v7.widget.AppCompatEditText {
    private TextInputLayout textInputLayout = null;
    private List<Validation> listValidate = new ArrayList<>();

    public EdittextValidate(Context context) {
        super(context);
    }

    public EdittextValidate(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EdittextValidate(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setupWithTextInputLayout(TextInputLayout textInputLayout) {
        this.textInputLayout = textInputLayout;
    }


    public void addValidation(final Validation... validations) {
        for (Validation validation : validations) {
            listValidate.add(validation);
        }

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                for (Validation validation : listValidate) {
                    if (!validation.validate(s.toString().trim())) {
                        if (textInputLayout != null) {
                            textInputLayout.setError(validation.getMessage());
                        } else {
                            setError(validation.getMessage());
                        }
                        return;
                    }
                }
                if (textInputLayout != null) {
                    textInputLayout.setError(null);
                }else{
                    setError(null);
                }
            }
        });
    }
    public boolean isValidate(){
        if(listValidate.isEmpty()){
            return true;
        }
        for (Validation validation : listValidate) {
            if (!validation.validate(getText().toString().trim())) {
                return false;
            }
        }
        return true;
    }
}
