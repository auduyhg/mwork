package com.example.company.mwork.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.adapter.LanguageSpokenArrayAdapter;
import com.example.company.mwork.databinding.FragmentRegisterTwoBinding;
import com.example.company.mwork.fragmentViewModel.FragmentRegisterTwoViewModel;
import com.example.company.mwork.utils.Constrant;
import com.example.company.mwork.utils.ValidationID;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterTwoFragment extends BaseFragment<FragmentRegisterTwoBinding, FragmentRegisterTwoViewModel> implements View.OnClickListener {


    public RegisterTwoFragment() {
        // Required empty public constructor
    }


    @Override
    public int getVariableId() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_register_two;
    }

    @Override
    public FragmentRegisterTwoViewModel setViewModel() {
        return new FragmentRegisterTwoViewModel(getActivity());
    }

    @Override
    public void onCreateFragment(Context context) {
        getBinding().lvLanguageSpoken.setAdapter(getViewModel().getAdapter());
        getBinding().lvLanguageSpoken.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (getViewModel().getAdapter().getObjects().get(position).isChecked()) {
                    getViewModel().getAdapter().getObjects().get(position).setChecked(false);
                } else {
                    getViewModel().getAdapter().getObjects().get(position).setChecked(true);
                }
                Toast.makeText(getActivity(), getViewModel().getAdapter().getListLanguageChecked().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        getBinding().ivUploadId.setOnClickListener(this);
        getBinding().edID.setupWithTextInputLayout(getBinding().tilID);
        getBinding().edID.addValidation(new ValidationID());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivUploadId:
                if (getViewModel().isUploadedImage()) {
                    getViewModel().setUploadedImage(false);
                } else {
                    getViewModel().setUploadedImage(true);
                }
                break;
        }
    }

    public boolean isValidate() {
        if (getBinding().edID.isValidate()
                && getViewModel().isUploadedImage()
                && !getViewModel().getAdapter().getListLanguageChecked().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
