package com.example.company.mwork.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.activityViewModel.ActivityStartViewModel;
import com.example.company.mwork.databinding.ActivityStartBinding;

public class StartActivity extends BaseActivity<ActivityStartBinding, ActivityStartViewModel> implements View.OnClickListener {


    @Override
    public int getLayoutId() {
        return R.layout.activity_start;
    }

    @Override
    public ActivityStartViewModel setViewModel() {
        return new ActivityStartViewModel();
    }

    @Override
    public int getVariableId() {
        return BR.viewModel;
    }

    @Override
    public void onCreateActivity() {
        setStatusBarTranslucent(true);
        getSupportActionBar().hide();
        setOnClick();
    }

    private void setOnClick() {
        getBinding().btnSignIn.setOnClickListener(this);
        getBinding().btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignIn:
                Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSignUp:
                Intent intent2 = new Intent(StartActivity.this, RegisterActivity.class);
                startActivity(intent2);
                break;

        }
    }
}
