package com.example.company.mwork.fragmentViewModel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.company.mwork.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.adapter.LanguageSpokenArrayAdapter;
import com.example.company.mwork.utils.Constrant;

/**
 * Created by Khuong Duy on 11/29/2017.
 */

public class FragmentRegisterTwoViewModel extends BaseObservable {
    private LanguageSpokenArrayAdapter adapter;
    private boolean isUploadedImage = false;

    public FragmentRegisterTwoViewModel(Context context) {
        this.adapter = new LanguageSpokenArrayAdapter(context, R.layout.item_language_spoken, Constrant.LANGUAGE_SPOKEN_LIST);;
    }

    public LanguageSpokenArrayAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(LanguageSpokenArrayAdapter adapter) {
        this.adapter = adapter;
    }
    @Bindable
    public boolean isUploadedImage() {
        return isUploadedImage;
    }

    public void setUploadedImage(boolean uploadedImage) {
        isUploadedImage = uploadedImage;
        notifyPropertyChanged(BR.uploadedImage);
    }
}
