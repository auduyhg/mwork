package com.example.company.mwork.utils;

/**
 * Created by Khuong Duy on 11/30/2017.
 */

public interface Validation {
    String getMessage();
    boolean validate(String input);
}
