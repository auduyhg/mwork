package com.example.company.mwork.utils;

/**
 * Created by Khuong Duy on 11/30/2017.
 */

public class ValidationID implements Validation {
    public static final int MIN_ID_LENGHT = 9;

    @Override
    public String getMessage() {
        return "Minimum ID length is " + MIN_ID_LENGHT;
    }

    @Override
    public boolean validate(String input) {
        return (input.length() >= MIN_ID_LENGHT);
    }
}
