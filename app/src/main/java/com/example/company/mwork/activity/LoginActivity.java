package com.example.company.mwork.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.activityViewModel.ActivityLoginViewModel;
import com.example.company.mwork.databinding.ActivityLoginBinding;
import com.example.company.mwork.utils.ValidationEmail;
import com.example.company.mwork.utils.ValidationPassword;

public class LoginActivity extends BaseActivity<ActivityLoginBinding, ActivityLoginViewModel> implements View.OnClickListener {


    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public ActivityLoginViewModel setViewModel() {
        return new ActivityLoginViewModel();
    }

    @Override
    public int getVariableId() {
        return BR.viewModel;
    }

    @Override
    public void onCreateActivity() {
        setStatusBarTranslucent(true);
        getSupportActionBar().hide();
        setOnClick();
        getBinding().edEmail.setupWithTextInputLayout(getBinding().tilEmail);
        getBinding().edEmail.addValidation(new ValidationEmail(), new ValidationPassword());
        getBinding().edPassword.setupWithTextInputLayout(getBinding().tilPassword);
        getBinding().edPassword.addValidation(new ValidationPassword());
    }

    private void setOnClick() {
        getBinding().btnCreateAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCreateAccount:
                Intent intent2 = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent2);
                break;

        }
    }
}
