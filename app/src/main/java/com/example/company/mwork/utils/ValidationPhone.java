package com.example.company.mwork.utils;

import android.text.TextUtils;

/**
 * Created by Khuong Duy on 11/30/2017.
 */

public class ValidationPhone implements Validation{
    @Override
    public String getMessage() {
        return "Incorrect phone number format";
    }

    @Override
    public boolean validate(String input) {
        if (input == null || TextUtils.isEmpty(input)) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(input).matches();
        }
    }
}
