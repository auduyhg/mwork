package com.example.company.mwork.activityViewModel;


import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v4.app.FragmentManager;

import com.example.company.mwork.BR;
import com.example.company.mwork.adapterViewPager.BaseFragmentPagerAdapter;
import com.example.company.mwork.fragment.BaseFragment;
import com.example.company.mwork.fragment.RegisterOneFragment;
import com.example.company.mwork.fragment.RegisterTwoFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khuong Duy on 11/29/2017.
 */

public class ActivityRegisterViewModel extends BaseObservable {
    private BaseFragmentPagerAdapter pagerAdapter;
    private boolean isShowCreateButton;

    public ActivityRegisterViewModel(FragmentManager fragmentManager) {
        pagerAdapter = new BaseFragmentPagerAdapter(fragmentManager);
        createRegisterStep();
    }
    private void createRegisterStep() {
        RegisterOneFragment fragment1 = new RegisterOneFragment();
        RegisterTwoFragment fragment2 = new RegisterTwoFragment();
        List<BaseFragment> list = new ArrayList<>();
        list.add(fragment1);
        list.add(fragment2);
        pagerAdapter.setList(list);
    }

    public BaseFragmentPagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }

    public void setPagerAdapter(BaseFragmentPagerAdapter pagerAdapter) {
        this.pagerAdapter = pagerAdapter;
    }
    @Bindable
    public boolean isShowCreateButton() {
        return isShowCreateButton;
    }

    public void setShowCreateButton(boolean showCreateButton) {
        isShowCreateButton = showCreateButton;
        notifyPropertyChanged(BR.showCreateButton);
    }
}
