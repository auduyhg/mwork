package com.example.company.mwork.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.example.company.mwork.R;
import com.example.company.mwork.adapter.BaseSingleTypeRecyclerViewAdapter;
import com.example.company.mwork.databinding.FragmentListJobsBinding;
import com.example.company.mwork.fragmentViewModel.FragmentListJobsViewModel;
import com.example.company.mwork.utils.Constrant;
import com.example.company.mwork.viewmodel.JobViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListJobsFragment extends BaseFragment<FragmentListJobsBinding, FragmentListJobsViewModel> {


    public ListJobsFragment() {
        // Required empty public constructor
    }


    @Override
    public int getVariableId() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_list_jobs;
    }

    @Override
    public FragmentListJobsViewModel setViewModel() {
        return new FragmentListJobsViewModel();
    }

    @Override
    public void onCreateFragment(Context context) {
        initRecyclerView();
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        getBinding().setLayoutManager(layoutManager);
        getViewModel().setAdapter(new BaseSingleTypeRecyclerViewAdapter<JobViewModel>(getActivity(), R.layout.item_list_jobs));
        getViewModel().getAdapter().setPresenter(new ItemAdapterClickListener());
        getBinding().setVariable(BR.adapter, getViewModel().getAdapter());
        getViewModel().setListOb(Constrant.DUMMY_LIST_JOBS);
    }

    public class ItemAdapterClickListener implements com.example.company.mwork.adapter.BaseRecyclerViewAdapter.Presenter {
        public void onItemClick(String values) {
            Toast.makeText(getActivity(), values, Toast.LENGTH_SHORT).show();
        }
    }
}
