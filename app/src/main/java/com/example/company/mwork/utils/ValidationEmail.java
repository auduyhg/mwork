package com.example.company.mwork.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Khuong Duy on 11/30/2017.
 */

public class ValidationEmail implements Validation {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    @Override
    public String getMessage() {
        return "Invalid Format Email";
    }

    @Override
    public boolean validate(String input) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(input);
        return matcher.find();
    }
}
